﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.Drawing;

namespace Multimedia
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string extension = "";
        string path = "";

        int flag = 0; // 1 : fano txt , 2 : fano img , 3 : lzw txt , 4 : lzw img
        byte[] compressedSh;
        string compressedLzw = "";

        public MainWindow()
        {
            InitializeComponent();
            saveButton.IsEnabled = false;
            lzwButton.IsEnabled = false;
            CompressButton.IsEnabled = false;

            // DecompressButton.IsEnabled = false;
            //string str = "This is an example for Shannon–Fano coding";
            //byte[] originalData = Encoding.Default.GetBytes(str);
            //uint originalDataSize = (uint)str.Length;
            //byte[] compressedData = new byte[originalDataSize * (101 / 100) + 384];

            //int compressedDataSize = Compresser.Compress(originalData, compressedData, originalDataSize);
        }

        private void loadBUtton_Click(object sender, RoutedEventArgs e)
        {


            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = "c:\\";
            dlg.Filter = "Images (*.BMP;*.GIF;*.PNG)|*.BMP;*.GIF;*.PNG|" + "txt files (*.txt)|*.txt|" + "All files (*.*)|*.*";
            dlg.RestoreDirectory = true;
            if (dlg.ShowDialog() == true)
            {
                path = dlg.FileName;
                extension = System.IO.Path.GetExtension(path);
            }
            List<string> ext = new List<string>();
            ext.Add(".bmp"); ext.Add(".gif"); ext.Add(".png"); ext.Add(".jpg"); ext.Add(".jpeg");

            if (ext.Contains(extension))
            {
                BitmapSource bSource = new BitmapImage(new Uri(path));
                //image.Source = bSource;
            }
            if (extension == ".lzw" || extension == ".shn")
                DecompressButton.IsEnabled = true;
            textBlock.Text = "the File loaded succefully from \n" + path;
            saveButton.IsEnabled = true;
            lzwButton.IsEnabled = true;
            CompressButton.IsEnabled = true;
        }

        private void CompressButton_Click(object sender, RoutedEventArgs e)
        {
            if (path == "" || extension == "")
                return;
            else
            {
                if (extension == ".txt")
                {
                    string fileName = path;
                    long size = 0;
                    FileInfo fi = new FileInfo(path);
                    if (fi.Exists)
                        size = fi.Length;//ababa text file size
                    byte[] bytes = Compresser.TextToByte(path);//ababa text to byte array
                    compressedSh = new byte[size * (101 / 100) + 384];
                    textBlock.Text += "\n Orginal Data Size " + size;
                    int compressedDataSize = Compresser.Compress(bytes, compressedSh, Convert.ToUInt32(size));

                    textBlock.Text += "\n Compressed Data Size " + compressedDataSize;
                    flag = 1;

                }
                else
                {

                    System.Drawing.Image img = System.Drawing.Image.FromFile(path);
                    byte[] bytes = Compresser.ImageToByte(img);
                    long size = bytes.Length;
                    compressedSh = new byte[size * (101 / 100) + 384];
                    textBlock.Text += "\n Orginal Data Size " + size;
                    int compressedDataSize = Compresser.Compress(bytes, compressedSh, Convert.ToUInt32(size));
                    textBlock.Text += "\n Compressed Data Size " + compressedDataSize;
                    flag = 2;


                }
                FileInfo fil = new FileInfo(path);
                textBlock.Text += "\n" + fil.Name + " Compressed successfully with Shannon Fano";
                lzwButton.IsEnabled = false;
                CompressButton.IsEnabled = false;
                /*
                SaveFileDialog dl = new SaveFileDialog();
                dl.ShowDialog();
                string str = dl.FileName;*/

            }
        }

        private void lzwButton_Click(object sender, RoutedEventArgs e)
        {


            if (path == "" || extension == "")
                return;
            else
            {
                if (extension == ".txt")
                {
                    string text = File.ReadAllText(path);
                    compressedLzw = LZString.LZString.Compress(text);
                    flag = 3;

                }
                else
                {
                    string str = Compresser.ImageToString(new System.Drawing.Bitmap(path));
                    compressedLzw = LZString.LZString.Compress(str);
                    flag = 4;

                }
                FileInfo fi = new FileInfo(path);
                textBlock.Text += "\n" + fi.Name + " Compressed successfully with LZW";
                lzwButton.IsEnabled = false;
                CompressButton.IsEnabled = false;
            }
        }

        private void DecompressButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            List<Object> result = new List<Object>();
            string extractPath = "";
            dlg.InitialDirectory = "c:\\";
            dlg.Filter = "Compressed Files (*.FANO;*.LZW)|*.FANO;*.LZW";
            dlg.RestoreDirectory = true;
            if (dlg.ShowDialog() == true)
            {
                extractPath = dlg.FileName;
            }


            using (Stream stream = File.Open(extractPath, FileMode.Open))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                result = (List<Object>)bformatter.Deserialize(stream);
            }
            if ((string)result[1] == "lzw_txt" || (string)result[1] == "lzw_img")
            {
                //extract lzw
                string tmp = (string)result[0];
                string res = LZString.LZString.Decompress(tmp);
                string res1 = LZString.LZString.Decompress(compressedLzw);
                if ((string)result[1] == "lzw_txt")
                {
                    //open save file dialog and write res to text file

                }
                else
                {
                    //open save file dialog and save res as image

                    byte[] bytes = Convert.FromBase64String(res1);

                    System.Drawing.Image img;
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        img = System.Drawing.Image.FromStream(ms);
                    }
                    BitmapSource source = Compresser.GetImageStream(img);
                    image.Source = source;
                }
            }
            
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            string savePath = "";
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = @"C:\";
            saveFileDialog1.Title = "Save Files";
            if (flag < 3)
            {
                saveFileDialog1.DefaultExt = "fano";
                saveFileDialog1.Filter = "Compressed Files (*.fano)|*.fano";
            }
            else if (flag >= 3)
            {
                saveFileDialog1.DefaultExt = "lzw";
                saveFileDialog1.Filter = "Compressed Files (*.lzw)|*.lzw";
            }
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == true)
            {
                savePath = saveFileDialog1.FileName;
            }

            switch (flag)
            {
                case 1://fano txt
                    {

                        using (Stream stream = File.Open(savePath, FileMode.Create))
                        {
                            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                            List<Object> list = new List<Object>();
                            list.Add(compressedSh);
                            list.Add("fano_txt");
                            bformatter.Serialize(stream, list);
                        }
                        break;
                    }
                case 2://fano img
                    {
                        using (Stream stream = File.Open(savePath, FileMode.Create))
                        {
                            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                            List<Object> list = new List<Object>();
                            list.Add(compressedSh);
                            list.Add("fano_img");
                            bformatter.Serialize(stream, list);
                        }

                        break;
                    }
                case 3://lzw txt
                    {
                        using (Stream stream = File.Open(savePath, FileMode.Create))
                        {
                            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                            List<Object> list = new List<Object>();
                            list.Add(compressedLzw);
                            list.Add("lzw_txt");
                            bformatter.Serialize(stream, list);
                        }
                        break;
                    }
                case 4://lzw img
                    {
                        using (Stream stream = File.Open(savePath, FileMode.Create))
                        {
                            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                            List<Object> list = new List<Object>();
                            list.Add(compressedLzw);
                            list.Add("lzw_img");
                            bformatter.Serialize(stream, list);
                        }

                        break;
                    }
            }
        }
    }
}