﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multimedia
{
    public class BitStream
    {
        public byte[] BytePointer;
        public uint BitPosition;
        public uint Index;
    }

}
